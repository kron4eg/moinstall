package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/labstack/echo"
	"github.com/shopspring/decimal"
)

type Campaign struct {
	Name       string          `json:"campaign_name"`
	Price      decimal.Decimal `json:"price"`
	TargetList []Target        `json:"target_list"`
}

type Target struct {
	Target   string   `json:"target"`
	AttrList []string `json:"attr_list"`
}

func NewRandomCampaign(rnd *rand.Rand, idx int) Campaign {
	c := Campaign{
		Name:  fmt.Sprintf("campaign%d", idx),
		Price: decimal.NewFromFloatWithExponent(rnd.ExpFloat64(), -2),
	}
	return c
}

func NewRandomTargetList(rnd *rand.Rand, targetLen int, attrLen int) []Target {
	tList := make([]Target, 0, targetLen)

	for i := 0; i < targetLen; i++ {
		attrs := make([]string, 0, attrLen)

		for y := 1; y <= attrLen; y++ {
			attrs = append(attrs, fmt.Sprintf("%c%d", 65+i, y))
		}

		tList = append(tList, Target{
			Target:   fmt.Sprintf("attr_%c", 65+i),
			AttrList: attrs,
		})
	}

	return tList
}

func GenerateCampaigns(c echo.Context) error {
	var f CampaignForm
	if err := c.Bind(&f); err != nil {
		return err
	}

	if err := f.Validate(); err != nil {
		return err
	}

	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	campNum := f.CampaignLen
	campaigns := make([]Campaign, 0, campNum)

	for i := 1; i <= campNum; i++ {
		cmp := NewRandomCampaign(rnd, i)
		cmp.TargetList = NewRandomTargetList(rnd, f.TargetLen, f.AttrLen)
		campaigns = append(campaigns, cmp)
	}

	return c.JSON(http.StatusOK, campaigns)
}

func ImportCampaign(c echo.Context) error {
	if err := DB.ImportJSON(c.Request().Body()); err != nil {
		return err
	}

	return c.String(http.StatusAccepted, fmt.Sprintf("%d", DB.Len()))
}
