package main

import (
	"encoding/json"
	"net/http"

	"github.com/labstack/echo"
)

func Search(c echo.Context) error {
	var user User
	dec := json.NewDecoder(c.Request().Body())
	if err := dec.Decode(&user); err != nil {
		return err
	}

	return c.JSON(http.StatusOK, DB.Search(GetQuery(user)))
}

func SearchAuto(c echo.Context) error {
	user := NewUser()

	return c.JSON(http.StatusOK, DB.Search(GetQuery(user)))
}

func GetQuery(u User) []string {
	query := make([]string, 0)
	for _, attr := range u.Profile {
		query = append(query, attr)
	}

	return query
}
