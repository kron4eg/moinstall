package main

const (
	errInvalidAttr     = Error("Invalid z")
	errInvalidTarget   = Error("Invalid y")
	errInvalidCampaign = Error("Invalid z")
)

type CampaignForm struct {
	AttrLen     int `json:"x" form:"x"`
	TargetLen   int `json:"y" form:"y"`
	CampaignLen int `json:"z" form:"z"`
}

func (f CampaignForm) Validate() error {
	if f.AttrLen > 100 || f.AttrLen <= 0 {
		return errInvalidAttr
	}
	if f.TargetLen > 26 || f.TargetLen <= 0 {
		return errInvalidTarget
	}
	if f.CampaignLen > 10000 || f.CampaignLen <= 0 {
		return errInvalidCampaign
	}

	return nil
}
