package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/engine/standard"
	"github.com/labstack/echo/middleware"
)

func main() {
	e := echo.New()
	e.SetDebug(true)
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/campaign", GenerateCampaigns)
	e.GET("/user", GenerateUser)
	e.POST("/import_camp", ImportCampaign)
	e.POST("/search", Search)
	e.GET("/search_auto", SearchAuto)

	e.Run(standard.New(":3000"))
}
