package main

import (
	"encoding/json"
	"io"
	"sync"
	"sync/atomic"
)

var (
	DB            = &DataBase{}
	searchCounter int32
)

type DataBase struct {
	data []Campaign
	idx  map[string][]int
	l    sync.RWMutex
}

func (d *DataBase) Len() int {
	d.l.RLock()
	defer d.l.RUnlock()

	return len(d.data)
}

func (d *DataBase) ImportJSON(r io.Reader) error {
	d.l.Lock()
	defer d.l.Unlock()

	dec := json.NewDecoder(r)

	if err := dec.Decode(&(d.data)); err != nil {
		return err
	}

	d.idx = make(map[string][]int)

	for idx, camp := range d.data {
		for _, target := range camp.TargetList {
			for _, attr := range target.AttrList {
				d.idx[attr] = append(d.idx[attr], idx)
			}
		}
	}

	return nil
}

func (d *DataBase) Search(query []string) SearchResult {
	d.l.RLock()
	defer d.l.RUnlock()

	var campResult Campaign

	for _, q := range query {
		campaignsIDX, ok := d.idx[q]
		if !ok {
			continue
		}

		for _, i := range campaignsIDX {
			camp := d.data[i]
			if camp.Price.Cmp(campResult.Price) == 1 {
				campResult = camp
			}
		}
	}

	if campResult.Name == "" {
		campResult.Name = "none"
	}

	return SearchResult{
		Winner:  campResult.Name,
		Counter: atomic.AddInt32(&searchCounter, 1),
		Query:   query,
	}
}

type SearchResult struct {
	Winner  string   `json:"winner"`
	Counter int32    `json:"counter"`
	Query   []string `json:"query"`
}
