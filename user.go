package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"sync/atomic"
	"time"

	"github.com/labstack/echo"
)

const (
	Letters        = 26
	ASCIIStart     = 65
	MaxUserAttrRnd = 200
)

var (
	userCounter int32
)

func NewUser() User {
	seq := atomic.AddInt32(&userCounter, 1)
	id := fmt.Sprintf("u%d", seq)
	attrNum := int(seq % Letters)
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))

	if attrNum == 0 {
		attrNum = Letters
	}

	u := User{
		ID:      id,
		Profile: make(map[string]string),
	}

	for i := 0; i < attrNum; i++ {
		profKey := fmt.Sprintf("attr_%c", ASCIIStart+i)
		u.Profile[profKey] = fmt.Sprintf("%c%d", ASCIIStart+i, rnd.Intn(MaxUserAttrRnd))
	}

	return u
}

type User struct {
	ID      string            `json:"user"`
	Profile map[string]string `json:"profile"`
}

func GenerateUser(c echo.Context) error {
	return c.JSON(http.StatusOK, NewUser())
}
